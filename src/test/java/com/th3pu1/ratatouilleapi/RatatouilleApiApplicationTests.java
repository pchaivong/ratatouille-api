package com.th3pu1.ratatouilleapi;

import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import com.th3pu1.ratatouilleapi.service.CategoryService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.data.repository.CrudRepository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RatatouilleApiApplicationTests {

	@Test
	public void contextLoads() {
	}

}
