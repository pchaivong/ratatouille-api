package com.th3pu1.ratatouilleapi.entity;

import com.th3pu1.ratatouilleapi.repository.CategoryRepository;
import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Before
    public void setUp(){

    }


    @Test
    public void test1AddCategory(){
        //Given
        Category test = new Category();
        test.setName("Test");
        test.setKitchenEnabled(true);
        Category saved = entityManager.persist(test);

        //When
        Category found = categoryRepository.findOne(saved.getId());

        // Expected
        assertThat(found.getName()).isEqualTo("Test");
        assertThat(found.getId()).isEqualTo(saved.getId());
        assertThat(found.isKitchenEnabled()).isTrue();
    }

    @Test
    public void test2UpdateCategoryName(){

        Category test = new Category();
        test.setName("Test");
        test.setKitchenEnabled(true);
        Category saved = entityManager.persist(test);

        Category category = categoryRepository.findOne(saved.getId());
        category.setName("Test2");
        categoryRepository.save(category);

        Category found = categoryRepository.findOne(saved.getId());

        assertThat(found.getName()).isEqualTo(category.getName());
        assertThat(found.isKitchenEnabled()).isTrue();
    }

    @Test
    public void test3DeleteCategory(){

        Category test = new Category();
        test.setName("Test");
        test.setKitchenEnabled(true);
        Category saved = entityManager.persist(test);

        categoryRepository.delete(saved.getId());

        assertThat(categoryRepository.count()).isEqualTo((long)0);

    }

}