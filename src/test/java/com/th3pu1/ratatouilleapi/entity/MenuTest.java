package com.th3pu1.ratatouilleapi.entity;

import com.th3pu1.ratatouilleapi.dto.CategoryResponse;
import com.th3pu1.ratatouilleapi.repository.CategoryRepository;
import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;



@RunWith(SpringRunner.class)
@DataJpaTest
public class MenuTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Test
    public void testGetMenuByCategory(){

        // Create two categories

        Category cat1 = new Category();
        Category cat2 = new Category();

        cat1.setName("Category1");
        cat1.setKitchenEnabled(true);

        cat2.setName("Category2");
        cat2.setKitchenEnabled(true);

        Category savedCat1 = categoryRepository.save(cat1);
        Category saveCate2 = categoryRepository.save(cat2);


        String[] cat1Menus = new String[] {"M1", "M2", "M3", "M4"};
        String[] cat2Menus = new String[] {"S1", "S2"};


        Arrays.stream(cat1Menus).forEach(name -> {
            Menu m = new Menu();
            m.setCategory(savedCat1);
            m.setName(name);
            menuRepository.save(m);
        });

        Arrays.stream(cat2Menus).forEach(name -> {
            Menu m = new Menu();
            m.setName(name);
            m.setCategory(saveCate2);
            menuRepository.save(m);
        });


        assertThat(menuRepository.getMenusByCategory(savedCat1.getId()).size())
                .isEqualTo(cat1Menus.length);
        assertThat(menuRepository.getMenusByCategory(saveCate2.getId()).size())
                .isEqualTo(cat2Menus.length);

    }


}