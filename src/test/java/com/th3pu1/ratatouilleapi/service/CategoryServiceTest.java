package com.th3pu1.ratatouilleapi.service;

import com.th3pu1.ratatouilleapi.dto.CategoryRequest;
import com.th3pu1.ratatouilleapi.dto.CategoryResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@Slf4j
public class CategoryServiceTest {

    @MockBean
    private CategoryService categoryService;


    @Test
    public void createCategory() throws Exception {
        CategoryRequest request = new CategoryRequest();
        request.setName("UnitTest");
        request.setKitchenEnabled(true);

        Optional<CategoryResponse> response = categoryService.createCategory(request);

        if (response == null)
            log.info("response is null");

      //  assertThat(response.isPresent()).isFalse();
    }

    @Test
    public void deleteCategory() throws Exception {
    }

    @Test
    public void updateCategory() throws Exception {
    }

}