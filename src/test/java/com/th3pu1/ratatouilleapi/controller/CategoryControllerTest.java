package com.th3pu1.ratatouilleapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.th3pu1.ratatouilleapi.dto.CategoryRequest;
import com.th3pu1.ratatouilleapi.service.CategoryService;
import com.th3pu1.ratatouilleapi.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@WebMvcTest(value = CategoryController.class, secure = false)
@Slf4j
public class CategoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoryController categoryController;

    @Test
    public void createCategory() throws Exception {
        CategoryRequest mockReq = new CategoryRequest();
        mockReq.setKitchenEnabled(false);
        mockReq.setName("UnitTest");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/api/categories"
        ).accept(MediaType.APPLICATION_JSON_VALUE);



        mockMvc.perform(post("/api/categories")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(mockReq))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
         //       .andExpect(jsonPath("$", hasSize(2)))
         //       .andExpect(jsonPath("$.name", is("UnitTest")))
         //       .andExpect(jsonPath("$.kitchen-enabled", is(true)));


    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            System.out.println(jsonContent);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}