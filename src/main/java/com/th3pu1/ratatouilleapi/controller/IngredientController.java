package com.th3pu1.ratatouilleapi.controller;


import com.th3pu1.ratatouilleapi.controller.exception.ResourceNotFoundException;
import com.th3pu1.ratatouilleapi.dto.IngredientRequest;
import com.th3pu1.ratatouilleapi.dto.IngredientResponse;
import com.th3pu1.ratatouilleapi.service.IngredientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
@CrossOrigin("*")
public class IngredientController {

    private IngredientService ingredientService;

    @Autowired
    public IngredientController(IngredientService ingredientService){
        this.ingredientService = ingredientService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = "/api/categories/{categoryId}/ingredients", method = RequestMethod.POST)
    public IngredientResponse createIngredient(@RequestBody IngredientRequest request,
                                               @PathVariable long categoryId){

        Optional<IngredientResponse> response = ingredientService.createIngredient(request, categoryId);
        return response.orElseThrow(ResourceNotFoundException::new);
    }

    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/api/ingredients/{id}", method = RequestMethod.DELETE)
    public void deleteIngredient(@PathVariable long id){
        ingredientService.deleteIngredient(id);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/ingredients/{id}", method = RequestMethod.PUT)
    public IngredientResponse updateIngredient(@RequestBody IngredientRequest request,
                                               @PathVariable long id){
        Optional<IngredientResponse> response = ingredientService.updateIngredient(request, id);
        return response.orElseThrow(ResourceNotFoundException::new);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/categories/{categoryId}/ingredients", method = RequestMethod.GET)
    public List<IngredientResponse> getIngredientListByCategory(@PathVariable long categoryId){
        return ingredientService.getIngredientListByCategory(categoryId);
    }
}
