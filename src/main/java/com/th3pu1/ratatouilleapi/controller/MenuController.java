package com.th3pu1.ratatouilleapi.controller;

import com.th3pu1.ratatouilleapi.controller.exception.ResourceNotFoundException;
import com.th3pu1.ratatouilleapi.dto.MenuRequest;
import com.th3pu1.ratatouilleapi.dto.MenuResponse;
import com.th3pu1.ratatouilleapi.service.CategoryService;
import com.th3pu1.ratatouilleapi.service.MenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin("*")
@RestController
@Slf4j
public class MenuController {

    private MenuService menuService;
    private CategoryService categoryService;

    @Autowired
    public MenuController(MenuService menuService,
                          CategoryService categoryService){
        this.categoryService = categoryService;
        this.menuService = menuService;
    }

    /**
     * Create new menu which belong to a given category
     * @param request
     * @param categoryId
     * @return
     */
    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = "/api/categories/{categoryId}/menus", method = RequestMethod.POST)
    public MenuResponse createMenu(@RequestBody MenuRequest request,
                                   @PathVariable long categoryId){
        Optional<MenuResponse> response = menuService.createMenu(request, categoryId);
        return response.orElseThrow(ResourceNotFoundException::new);
    }

    /**
     * Get menu list by given category
     * @param categoryId
     * @return
     */
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/categories/{categoryId}/menus", method = RequestMethod.GET)
    public List<MenuResponse> getMenuList(@PathVariable long categoryId){
        return menuService.getMenuListByCategory(categoryId);
    }

    /**
     * Delete menu by given menuId.
     * @param menuId
     */
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/api/menus/{menuId}", method = RequestMethod.DELETE)
    public void deleteMenu(@PathVariable long menuId){
        menuService.deleteMenu(menuId);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/menus/{menuId}", method = RequestMethod.PUT)
    public MenuResponse updateMenuDetail(@RequestBody MenuRequest request,
                                         @PathVariable long menuId){
        return menuService.updateMenu(request, menuId)
                .orElseThrow(ResourceNotFoundException::new);
    }
}
