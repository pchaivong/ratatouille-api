package com.th3pu1.ratatouilleapi.controller;


import com.th3pu1.ratatouilleapi.controller.exception.ResourceNotFoundException;
import com.th3pu1.ratatouilleapi.dto.CategoryRequest;
import com.th3pu1.ratatouilleapi.dto.CategoryResponse;
import com.th3pu1.ratatouilleapi.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CategoryController {

    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    /**
     * Create new category
     * @param request
     * @return
     */
    @CrossOrigin("*")
    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = "/api/categories", method = RequestMethod.POST)
    public CategoryResponse createCategory(@RequestBody CategoryRequest request){
        Optional<CategoryResponse> response = categoryService.createCategory(request);
        return response.orElseThrow(ResourceNotFoundException::new);
    }


    /**
     * Delete given category
     * @param id
     */
    @CrossOrigin("*")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @RequestMapping(value = "/api/categories/{id}", method = RequestMethod.DELETE)
    public void deleteCategory(@PathVariable long id){
        categoryService.deleteCategory(id);
    }

    /**
     * Update category detail
     * @param request
     * @param id
     * @return
     */
    @CrossOrigin("*")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/categories/{id}", method = RequestMethod.PUT)
    public CategoryResponse updateCategory(@RequestBody CategoryRequest request,
                                           @PathVariable long id){

        return categoryService.updateCategory(request, id).orElseThrow(ResourceNotFoundException::new);
    }

    /**
     * Get category detail
     * @param id
     * @return
     */
    @CrossOrigin("*")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/categories/{id}", method = RequestMethod.GET)
    public CategoryResponse getCategoryDetail(@PathVariable long id){
        Optional<CategoryResponse> response = categoryService.getCategoryDetail(id);
        return response.orElseThrow(ResourceNotFoundException::new);

    }

    /**
     * Get category list
     * @return
     */
    @CrossOrigin("*")
    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/categories", method = RequestMethod.GET)
    public List<CategoryResponse> getCategoryList(){
        return categoryService.getCategoryList();
    }
}
