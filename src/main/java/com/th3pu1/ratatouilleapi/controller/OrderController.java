package com.th3pu1.ratatouilleapi.controller;

import com.th3pu1.ratatouilleapi.controller.exception.ResourceNotFoundException;
import com.th3pu1.ratatouilleapi.dto.OrderDetailRequest;
import com.th3pu1.ratatouilleapi.dto.OrderDetailResponse;
import com.th3pu1.ratatouilleapi.dto.OrderRequest;
import com.th3pu1.ratatouilleapi.dto.OrderResponse;
import com.th3pu1.ratatouilleapi.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Slf4j
@CrossOrigin("*")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService = orderService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = "/api/orders", method = RequestMethod.POST)
    public OrderResponse createOrder(@RequestBody OrderRequest request){
        Optional<OrderResponse> response = orderService.createOrder(request);
        return response.orElseThrow(ResourceNotFoundException::new);
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @RequestMapping(value = "/api/orders/{orderId}/details", method = RequestMethod.POST)
    public OrderDetailResponse addOrderDetail(@RequestBody OrderDetailRequest request,
                                              @PathVariable long orderId){
        Optional<OrderDetailResponse> response = orderService.addOrderDetail(request, orderId);
        return response.orElseThrow(ResourceNotFoundException::new);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/api/orders/{orderId}", method = RequestMethod.GET)
    public OrderResponse getOrder(@PathVariable long orderId){
        Optional<OrderResponse> response = orderService.getOrder(orderId);
        return response.orElseThrow(ResourceNotFoundException::new);
    }
}
