package com.th3pu1.ratatouilleapi.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "INGREDIENT")
public class Ingredient {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COST_PER_UNIT")
    private BigDecimal costPerUnit;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "TAG")
    private String tag;

    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

}
