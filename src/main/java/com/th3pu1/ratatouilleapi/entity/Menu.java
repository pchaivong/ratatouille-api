package com.th3pu1.ratatouilleapi.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "MENU")
public class Menu {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    private Category category;

    @Transient
    private List<Ingredient> ingredients = new ArrayList<>();

    @Transient
    private List<PriceSize> sizes = new ArrayList<>();

    @OneToMany(mappedBy = "menu", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    public List<PriceSize> getSizes(){ return sizes; }

    @ManyToMany
    @JoinTable(name = "MENU_INGREDIENT", joinColumns = @JoinColumn(name = "MENU_ID", referencedColumnName = "ID"),
    inverseJoinColumns = @JoinColumn(name = "INGREDIENT_ID", referencedColumnName = "ID"))
    public List<Ingredient> getIngredients(){
        return ingredients;
    }


    public void addSize(PriceSize s){
        sizes.add(s);
    }

    public void addIngredient(Ingredient ingredient){
        ingredients.add(ingredient);
    }

}
