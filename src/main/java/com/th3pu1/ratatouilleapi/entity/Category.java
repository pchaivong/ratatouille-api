package com.th3pu1.ratatouilleapi.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "CATEGORY")
public class Category {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;


    @Column(name = "NAME")
    private String name;

    @Column(name = "KITCHEN_ENABLED")
    private boolean kitchenEnabled;

    @Transient
    private List<Menu> menus = new ArrayList<>();

    @Transient
    private List<Ingredient> ingredients = new ArrayList<>();


    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    public List<Menu> getMenus(){
        return menus;
    }


    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
    public List<Ingredient> getIngredients() { return ingredients; }
}
