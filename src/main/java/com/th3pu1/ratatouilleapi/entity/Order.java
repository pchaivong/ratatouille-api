package com.th3pu1.ratatouilleapi.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "MENU_ORDER")
public class Order {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "TABLE_LABEL")
    private String table;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "TOTAL_PRICE")
    private BigDecimal totalPrice;

    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails = new ArrayList<>();


    /*
    @OneToMany(mappedBy = "order", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public List<OrderDetail> getOrderDetails(){ return orderDetails; }
    */

    public void addOrderDetail(OrderDetail detail){
        orderDetails.add(detail);
    }



}
