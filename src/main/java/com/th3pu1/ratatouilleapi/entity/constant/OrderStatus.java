package com.th3pu1.ratatouilleapi.entity.constant;

public class OrderStatus {

    public static final String CREATED = "CREATED";
    public static final String COOKING = "COOKING";
    public static final String SERVED = "SERVED";
    public static final String CHECKED = "CHECKED";
}
