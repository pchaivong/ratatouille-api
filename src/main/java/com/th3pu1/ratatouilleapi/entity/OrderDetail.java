package com.th3pu1.ratatouilleapi.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "ORDER_DETAIL")
public class OrderDetail {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "TAKE_AWAY")
    private boolean takeAway;

    @Column(name = "AMOUNT")
    private long amount;

    @ManyToOne(fetch = FetchType.EAGER)
    private Order order;

    @ManyToOne(fetch = FetchType.EAGER)
    private Menu menu;

}
