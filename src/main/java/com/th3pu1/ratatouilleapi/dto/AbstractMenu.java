package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class AbstractMenu {

    @JsonProperty("name")
    private String name;
}
