package com.th3pu1.ratatouilleapi.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderResponse extends AbstractOrder{

    @JsonProperty("id")
    private long id;

    @JsonProperty("status")
    private String status;

    @JsonProperty("order-details")
    private List<OrderDetailResponse> orderDetails = new ArrayList<>();

    @JsonProperty("total-price")
    private BigDecimal totalPrice;


    public void addOrderDetail(OrderDetailResponse detail){
        orderDetails.add(detail);
    }
}
