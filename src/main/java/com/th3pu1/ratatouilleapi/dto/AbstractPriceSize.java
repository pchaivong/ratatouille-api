package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Slf4j
@ToString
public abstract class AbstractPriceSize {

    @JsonProperty("name")
    private String name;

    @JsonProperty("price")
    private BigDecimal price;
}
