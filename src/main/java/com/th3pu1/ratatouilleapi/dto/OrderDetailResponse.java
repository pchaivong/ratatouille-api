package com.th3pu1.ratatouilleapi.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderDetailResponse extends OrderDetailRequest {

    @JsonProperty("id")
    private long id;
    
}
