package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@ToString
public class OrderDetailRequest {

    @JsonProperty("take-away")
    private boolean takeAway;

    @JsonProperty("menu-id")
    private long menuId;

    @JsonProperty("add-ons")
    private List<Long> addOns;

    @JsonProperty("amount")
    private Long amount;
}
