package com.th3pu1.ratatouilleapi.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CategoryRequest {

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty("kitchen-enabled")
    private boolean kitchenEnabled;
}
