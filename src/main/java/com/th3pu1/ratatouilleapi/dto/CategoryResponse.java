package com.th3pu1.ratatouilleapi.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class CategoryResponse extends CategoryRequest {

    @JsonProperty(value = "id")
    private long id;

    @JsonProperty(value = "menu-items")
    private int menuItems;

    @JsonProperty(value = "ingredient-items")
    private int ingredientItems;

}
