package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredientResponse extends AbstractIngredient {

    @JsonProperty("id")
    private long id;
}
