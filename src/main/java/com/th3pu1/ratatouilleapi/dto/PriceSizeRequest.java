package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@Slf4j
@ToString
public class PriceSizeRequest extends AbstractPriceSize {

}
