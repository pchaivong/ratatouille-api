package com.th3pu1.ratatouilleapi.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MenuRequest extends AbstractMenu{

    @JsonProperty("sizes")
    private List<PriceSizeRequest> sizes;

    @JsonProperty("ingredients")
    private List<Long> ingredients = new ArrayList<>();
}
