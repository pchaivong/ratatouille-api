package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Setter
@ToString
public abstract class AbstractIngredient {

    @JsonProperty("name")
    private String name;

    @JsonProperty("cost-per-unit")
    private BigDecimal costPerUnit;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("price")
    private BigDecimal price;

}
