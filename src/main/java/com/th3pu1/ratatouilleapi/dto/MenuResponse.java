package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MenuResponse extends AbstractMenu{

    @JsonProperty("id")
    private long id;

    @JsonProperty("sizes")
    private List<PriceSizeResponse> sizes = new ArrayList<>();

    @JsonProperty("ingredients")
    private List<IngredientResponse> ingredients = new ArrayList<>();

    public void addSize(PriceSizeResponse s){
        sizes.add(s);
    }

    public void addIngredient(IngredientResponse i) { ingredients.add(i); }

}
