package com.th3pu1.ratatouilleapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import lombok.extern.slf4j.Slf4j;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Slf4j
@ToString
public class PriceSizeResponse extends AbstractPriceSize {

    @JsonProperty("id")
    private long id;
}
