package com.th3pu1.ratatouilleapi.service;

import com.th3pu1.ratatouilleapi.dto.OrderDetailRequest;
import com.th3pu1.ratatouilleapi.dto.OrderDetailResponse;
import com.th3pu1.ratatouilleapi.dto.OrderRequest;
import com.th3pu1.ratatouilleapi.dto.OrderResponse;
import com.th3pu1.ratatouilleapi.entity.Menu;
import com.th3pu1.ratatouilleapi.entity.Order;
import com.th3pu1.ratatouilleapi.entity.OrderDetail;
import com.th3pu1.ratatouilleapi.entity.constant.OrderStatus;
import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import com.th3pu1.ratatouilleapi.repository.OrderDetailRepository;
import com.th3pu1.ratatouilleapi.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

@Service
@Slf4j
public class OrderService {

    private OrderRepository orderRepository;
    private OrderDetailRepository orderDetailRepository;
    private MenuRepository menuRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository,
                        OrderDetailRepository orderDetailRepository,
                        MenuRepository menuRepository){

        this.orderDetailRepository = orderDetailRepository;
        this.orderRepository = orderRepository;
        this.menuRepository = menuRepository;
    }


    public Optional<OrderResponse> createOrder(OrderRequest request){
        Order order = deserialized(request);
        order.setStatus(OrderStatus.CREATED);
        order.setTotalPrice(BigDecimal.ZERO);

        Order saved = orderRepository.save(order);
        if (saved == null){
            log.error("Cannot persist order");
            return Optional.empty();
        }

        return Optional.of(serialized(saved));
    }

    public Optional<OrderDetailResponse> addOrderDetail(OrderDetailRequest request, long orderId){
        Order order = orderRepository.findOne(orderId);
        if (order == null){
            log.info("Order not found");
            return Optional.empty();
        }
        OrderDetail detail = OrderDetailFromRequest(request);
        detail.setOrder(order);

        Menu menu = menuRepository.findOne(request.getMenuId());
        if (menu == null){
            log.info("Menu not found");
            return Optional.empty();
        }

        detail.setMenu(menu);

        OrderDetail saved = orderDetailRepository.save(detail);

        if (saved == null){
            log.error("Cannot persist order");
            return Optional.empty();
        }

        return Optional.of(OrderDetailToResponse(saved));
    }

    public Optional<OrderResponse> getOrder(long orderId){
        Order order = orderRepository.findOne(orderId);
        if (order == null){
            log.info("Order not found");
            return Optional.empty();
        }

        return Optional.of(serialized(order));
    }

    public static Order deserialized(OrderRequest request){
        Order order = new Order();
        order.setTable(request.getTable());
        return order;
    }

    public static OrderResponse serialized(Order order){
        OrderResponse response = new OrderResponse();
        response.setId(order.getId());
        response.setTable(order.getTable());
        response.setStatus(order.getStatus());
        response.setTotalPrice(order.getTotalPrice());

        order.getOrderDetails()
                .forEach(i -> {
                    response.addOrderDetail(OrderDetailToResponse(i));
                });
        return response;
    }

    public static OrderDetail OrderDetailFromRequest(OrderDetailRequest request){
        OrderDetail detail = new OrderDetail();
        detail.setTakeAway(request.isTakeAway());
        detail.setAmount(request.getAmount());
        return detail;
    }

    public static OrderDetailResponse OrderDetailToResponse(OrderDetail detail){
        OrderDetailResponse response = new OrderDetailResponse();
        response.setId(detail.getId());
        response.setTakeAway(detail.isTakeAway());
        response.setMenuId(detail.getMenu().getId());
        log.info("Order: " + detail.getOrder().getId());
        return response;
    }
}
