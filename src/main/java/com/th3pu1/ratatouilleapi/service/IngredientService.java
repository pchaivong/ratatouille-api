package com.th3pu1.ratatouilleapi.service;


import com.th3pu1.ratatouilleapi.dto.IngredientRequest;
import com.th3pu1.ratatouilleapi.dto.IngredientResponse;
import com.th3pu1.ratatouilleapi.entity.Category;
import com.th3pu1.ratatouilleapi.entity.Ingredient;
import com.th3pu1.ratatouilleapi.repository.CategoryRepository;
import com.th3pu1.ratatouilleapi.repository.IngredientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class IngredientService {

    private IngredientRepository ingredientRepository;
    private CategoryRepository categoryRepository;

    @Autowired
    public IngredientService(IngredientRepository ingredientRepository,
                             CategoryRepository categoryRepository){
        this.ingredientRepository = ingredientRepository;
        this.categoryRepository = categoryRepository;
    }

    public Optional<IngredientResponse> createIngredient(IngredientRequest request, long categoryId){
        Category category = categoryRepository.findOne(categoryId);
        if (category == null){
            log.info("Category not found");
            return Optional.empty();
        }

        Ingredient ingredient = deserialized(request);
        ingredient.setCategory(category);

        Ingredient saved = ingredientRepository.save(ingredient);

        if (saved == null){
            log.error("Cannot persist ingredient");
            return Optional.empty();
        }

        return Optional.of(serialized(saved));
    }

    public void deleteIngredient(long id){
        ingredientRepository.delete(id);
    }

    public Optional<IngredientResponse> updateIngredient(IngredientRequest request, long id){
        Ingredient ingredient = ingredientRepository.findOne(id);
        if (ingredient == null){
            log.info("Ingredient not found");
            return Optional.empty();
        }

        ingredient.setName(request.getName());
        ingredient.setPrice(request.getPrice());
        ingredient.setCostPerUnit(request.getCostPerUnit());
        ingredient.setTag(request.getTag());

        Ingredient updated = ingredientRepository.save(ingredient);
        if (updated == null){
            log.error("Cannot persist ingredient");
            return Optional.empty();
        }
        return Optional.of(serialized(updated));
    }

    public List<IngredientResponse> getIngredientListByCategory(long categoryId){
        List<IngredientResponse> items = new ArrayList<>();
        ingredientRepository.findIngredientsByCategory(categoryId)
                .forEach(i->{
                    items.add(serialized(i));
                });

        return items;
    }


    private IngredientResponse serialized(Ingredient entity){
        IngredientResponse response = new IngredientResponse();
        response.setId(entity.getId());
        response.setName(entity.getName());
        response.setCostPerUnit(entity.getCostPerUnit());
        response.setPrice(entity.getPrice());
        response.setTag(entity.getTag());

        return response;
    }

    private Ingredient deserialized(IngredientRequest request){
        Ingredient entity = new Ingredient();
        entity.setName(request.getName());
        entity.setPrice(request.getPrice());
        entity.setCostPerUnit(request.getCostPerUnit());
        entity.setTag(request.getTag());

        return entity;
    }
}
