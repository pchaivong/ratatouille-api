package com.th3pu1.ratatouilleapi.service;


import com.th3pu1.ratatouilleapi.dto.*;
import com.th3pu1.ratatouilleapi.entity.Category;
import com.th3pu1.ratatouilleapi.entity.Ingredient;
import com.th3pu1.ratatouilleapi.entity.Menu;
import com.th3pu1.ratatouilleapi.entity.PriceSize;
import com.th3pu1.ratatouilleapi.repository.CategoryRepository;
import com.th3pu1.ratatouilleapi.repository.IngredientRepository;
import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class MenuService {

    private MenuRepository menuRepository;
    private CategoryRepository categoryRepository;
    private IngredientRepository ingredientRepository;

    @Autowired
    public MenuService(MenuRepository menuRepository,
                       CategoryRepository categoryRepository,
                       IngredientRepository ingredientRepository){
        this.menuRepository = menuRepository;
        this.categoryRepository = categoryRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Create menu and add to existing category.
     * @param request
     * @param categoryId
     * @return
     */
    public Optional<MenuResponse> createMenu(MenuRequest request, long categoryId){
        Category category = categoryRepository.findOne(categoryId);
        if (category == null){
            log.info("Category id: " + categoryId + " is not found.");
            return Optional.empty();
        }

        Menu menu = deserialized(request);
        menu.setCategory(category);

        Menu saved = menuRepository.save(menu);

        if (saved == null)
            return Optional.empty();

        return Optional.of(serialized(saved));
    }

    public void deleteMenu(long id){
        menuRepository.delete(id);
    }


    /**
     * Update menu information. Name, ...
     * @param request
     * @param id
     * @return
     */
    public Optional<MenuResponse> updateMenu(MenuRequest request, long id){
        Menu menu = menuRepository.findOne(id);
        if (menu == null){
            log.info("Menu not found");
            return Optional.empty();
        }

        menu.setName(request.getName());

        request.getIngredients().forEach(i -> {
            Ingredient ingredient = ingredientRepository.findOne(i);
            if (ingredient != null){
                menu.addIngredient(ingredient);
            }
        });

        Menu saved = menuRepository.save(menu);
        if (saved == null){
            log.info("Cannot update Menu");
            return Optional.empty();
        }

        return Optional.of(serialized(saved));
    }


    public List<MenuResponse> getMenuListByCategory(long categoryId){
        List<MenuResponse> menus = new ArrayList<>();

        menuRepository.getMenusByCategory(categoryId)
                .forEach(m -> {
                    menus.add(serialized(m));
                });

        return menus;
    }



    private MenuResponse serialized(Menu entity){
        MenuResponse response = new MenuResponse();
        response.setId(entity.getId());
        response.setName(entity.getName());

        List<PriceSizeResponse> sizes = new ArrayList<>();
        entity.getSizes().forEach(s -> {
            PriceSizeResponse size = new PriceSizeResponse();
            size.setName(s.getName());
            size.setPrice(s.getPrice());
            size.setId(s.getId());
            response.addSize(size);
        });

        entity.getIngredients().forEach(i -> {
            IngredientResponse ingredient = new IngredientResponse();
            ingredient.setId(i.getId());
            ingredient.setName(i.getName());
            ingredient.setTag(i.getTag());
            ingredient.setPrice(i.getPrice());
            ingredient.setCostPerUnit(i.getCostPerUnit());
            response.addIngredient(ingredient);
        });

        return response;
    }

    private Menu deserialized(MenuRequest request){
        Menu menu = new Menu();
        menu.setName(request.getName());

        request.getSizes().forEach(s -> {
            PriceSize size = new PriceSize();
            size.setName(s.getName());
            size.setPrice(s.getPrice());
            size.setMenu(menu);
            menu.addSize(size);
        });

        request.getIngredients().forEach(i -> {
            Ingredient ingredient = ingredientRepository.findOne(i);
            if (ingredient != null){
                menu.addIngredient(ingredient);
            }
        });

        log.info("Deserialized: size count: " + menu.getSizes().size());

        return menu;
    }
}
