package com.th3pu1.ratatouilleapi.service;


import com.th3pu1.ratatouilleapi.dto.CategoryRequest;
import com.th3pu1.ratatouilleapi.dto.CategoryResponse;
import com.th3pu1.ratatouilleapi.entity.Category;
import com.th3pu1.ratatouilleapi.repository.CategoryRepository;
import com.th3pu1.ratatouilleapi.repository.IngredientRepository;
import com.th3pu1.ratatouilleapi.repository.MenuRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CategoryService {

    private CategoryRepository categoryRepository;
    private MenuRepository menuRepository;
    private IngredientRepository ingredientRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository,
                           MenuRepository menuRepository,
                           IngredientRepository ingredientRepository){

        this.categoryRepository = categoryRepository;
        this.menuRepository = menuRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * Create category
     * @param request
     * @return
     */
    public Optional<CategoryResponse> createCategory(CategoryRequest request){
        Category entity = deserialized(request);
        Category saved = categoryRepository.save(entity);
        if (saved == null)
            return Optional.empty();

        return Optional.of(serialized(saved));
    }


    /**
     * Delete category
     * @param id
     */
    public void deleteCategory(long id){
        categoryRepository.delete(id);
    }


    /**
     * Update category information
     * @param request
     * @param id
     * @return
     */
    public Optional<CategoryResponse> updateCategory(CategoryRequest request, long id){
        Category category = categoryRepository.findOne(id);

        if (category == null)
            return Optional.empty();

        // Can only update only these attributes
        category.setName(request.getName());
        category.setKitchenEnabled(request.isKitchenEnabled());

        Category saved = categoryRepository.save(category);

        return Optional.ofNullable(serialized(saved));
    }

    /**
     * Get Category detail.
     * @param id
     * @return
     */
    public Optional<CategoryResponse> getCategoryDetail(long id){
        Category category = categoryRepository.findOne(id);
        if (category == null){
            log.info("Category not found [id: " + id + "]");
            return Optional.empty();
        }

        return Optional.of(serialized(category));
    }

    /**
     * Get list of all categories.
     * @return
     */
    public List<CategoryResponse> getCategoryList(){
        List<CategoryResponse> items = new ArrayList<>();

        categoryRepository.findAll().forEach(item -> {
            items.add(serialized(item));
        });

        return items;
    }


    /**
     * Get number of menus that belong to given category
     * @param category
     * @return amount of menus for a given category
     */
    public int getMenuAmount(Category category){
        return menuRepository.getMenusByCategory(category.getId()).size();
    }


    private Category deserialized(CategoryRequest request){
        Category category = new Category();
        category.setName(request.getName());
        category.setKitchenEnabled(request.isKitchenEnabled());
        return category;
    }

    private CategoryResponse serialized(Category entity){
        if (entity == null)
            return null;

        CategoryResponse response = new CategoryResponse();
        response.setName(entity.getName());
        response.setId(entity.getId());
        response.setKitchenEnabled(entity.isKitchenEnabled());
        response.setMenuItems(menuRepository.getMenusByCategory(entity.getId()).size());
        response.setIngredientItems(ingredientRepository.findIngredientsByCategory(entity.getId()).size());

        return response;
    }
}
