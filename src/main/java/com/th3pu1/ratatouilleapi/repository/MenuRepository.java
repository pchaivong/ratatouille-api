package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.Menu;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MenuRepository extends CrudRepository<Menu, Long> {

    @Query("SELECT m FROM Menu m WHERE m.category.id = :categoryId")
    List<Menu> getMenusByCategory(@Param("categoryId")long categoryId);
}
