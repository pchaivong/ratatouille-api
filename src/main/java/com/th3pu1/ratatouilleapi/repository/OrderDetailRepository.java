package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.OrderDetail;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderDetailRepository extends CrudRepository<OrderDetail, Long> {

    @Query("SELECT o FROM OrderDetail o WHERE o.order.id = :orderId")
    List<OrderDetail> findOrderDetailsByOrder(@Param("orderId") long orderId);
}
