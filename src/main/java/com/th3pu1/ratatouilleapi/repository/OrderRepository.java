package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
