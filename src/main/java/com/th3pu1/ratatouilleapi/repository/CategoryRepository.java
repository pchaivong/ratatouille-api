package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
