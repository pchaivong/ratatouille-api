package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.Ingredient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IngredientRepository extends CrudRepository<Ingredient, Long> {

    @Query("SELECT i from Ingredient i WHERE i.category.id = :categoryId")
    List<Ingredient> findIngredientsByCategory(@Param("categoryId") long categoryId);
}
