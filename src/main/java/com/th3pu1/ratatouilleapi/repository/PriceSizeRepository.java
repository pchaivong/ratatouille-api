package com.th3pu1.ratatouilleapi.repository;

import com.th3pu1.ratatouilleapi.entity.PriceSize;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PriceSizeRepository extends CrudRepository<PriceSize, Long> {

    @Query("SELECT p FROM PriceSize p WHERE p.menu.id = :menuId")
    List<PriceSize> getSizesByMenuId(@Param("menuId")long menuId);
}
